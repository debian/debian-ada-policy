with Ada.Text_IO;

package body Hello_World is

   procedure Say_It is
   begin
      Ada.Text_IO.Put_Line ("Hello world!");
   end Say_It;

end Hello_World;
